<?php declare(strict_types=1);

namespace Palmyr\Phar\Builder;

interface BuilderInterface
{

    public function build(BuildConfigInterface $config): void;

}