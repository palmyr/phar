<?php

namespace Palmyr\Phar\Command;

use Palmyr\Functions\CommonUtils\Foo\Foo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PharBuildCommand extends Command
{

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $foo = new Foo();
        $output->writeln($foo->foo());
        return self::SUCCESS;
    }
}